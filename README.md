# [魔方栈](https://huazhechen.gitee.io/cuber)

- 魔方栈是一个在线魔方工具箱
- 虚拟魔方/公式播放/动画制作

## 基于以下项目

- threejs
- vue
- vuetify

## 联系方式

- QQ: 37705123
- email: 37705123@qq.com

# 功能介绍

## 虚拟魔方

- 极致触摸操控体验

  ![touch](https://gitee.com/huazhechen/cuber/raw/master/screenshot/touch.gif)

- 全功能键盘

  ![keyboard](https://gitee.com/huazhechen/cuber/raw/master/screenshot/keyboard.gif)

- 镜像助手

  ![mirror](https://gitee.com/huazhechen/cuber/raw/master/screenshot/mirror.gif)

- 涂色助手

  ![cf](https://gitee.com/huazhechen/cuber/raw/master/screenshot/cf.gif)

- 撤销操作

  ![backspace](https://gitee.com/huazhechen/cuber/raw/master/screenshot/backspace.gif)

- 重新打乱

  ![random](https://gitee.com/huazhechen/cuber/raw/master/screenshot/random.gif)

- 外观自定义

  ![tune](https://gitee.com/huazhechen/cuber/raw/master/screenshot/tune.gif)

## 公式播放

- 播放控制

  ![alg-player](https://gitee.com/huazhechen/cuber/raw/master/screenshot/alg-player.gif)

- 单步播放

  ![alg-step](https://gitee.com/huazhechen/cuber/raw/master/screenshot/alg-step.gif)


- 公式跳转

  ![alg-choose](https://gitee.com/huazhechen/cuber/raw/master/screenshot/alg-choose.gif)

- 快捷公式跳转

  ![alg-list](https://gitee.com/huazhechen/cuber/raw/master/screenshot/alg-list.gif)

- 公式修改

  ![alg-modify](https://gitee.com/huazhechen/cuber/raw/master/screenshot/alg-modify.gif)

## 动画制作

- 场景布置与截图

  ![snap](https://gitee.com/huazhechen/cuber/raw/master/screenshot/snap.gif)

- 动画编写与播放

  ![action](https://gitee.com/huazhechen/cuber/raw/master/screenshot/action.gif)

- 导出 gif

  ![gif](https://gitee.com/huazhechen/cuber/raw/master/screenshot/gif.gif)

- 自动布置场景

  ![reverse](https://gitee.com/huazhechen/cuber/raw/master/screenshot/reverse.gif)

- 自由涂色

  ![colorize](https://gitee.com/huazhechen/cuber/raw/master/screenshot/colorize.gif)

- 节奏控制

  ![movie-pause](https://gitee.com/huazhechen/cuber/raw/master/screenshot/movie-pause.gif)

- 质量配置

  ![movie-quality](https://gitee.com/huazhechen/cuber/raw/master/screenshot/movie-quality.gif)
