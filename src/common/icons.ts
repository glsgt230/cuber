export var ICONS = {
  backspace: require("../resource/backspace.svg"),
  mirror: require("../resource/mirror.svg"),
  colorize: require("../resource/colorize.svg"),
  layer1: require("../resource/layer1.svg"),
  layer2: require("../resource/layer2.svg"),
  layer3: require("../resource/layer3.svg"),
  cfop1: require("../resource/cfop1.svg"),
  cfop2: require("../resource/cfop2.svg"),
  hollow: require("../resource/hollow.svg"),
  lock: require("../resource/lock.svg")
};
